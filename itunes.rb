#! /usr/bin/env ruby
require 'bundler/setup'
require 'plist'

def home
  ENV['HOME']
end

target = "#{home}/Music/iTunes/iTunes\ Music\ Library.xml"
data = Plist::parse_xml(target)
artists = []

data['Tracks'].each do |_, val|
  val.each do |k, v|
    if k == 'Artist' && !v.empty?
      artists << v
    end
  end
end

artists.uniq!

puts artists
puts "Found #{artists.size} Artists"
#
File.open('output_itunes.csv', 'w') do |f|
  begin
    f.puts(artists)
  rescue => e
    puts 'error'
    puts e
  end
end
