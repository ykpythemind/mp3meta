#! /usr/bin/env ruby

require 'bundler/setup'
require 'audioinfo'
require 'find'

def home
  ENV['HOME']
end

roots = ["#{home}/Music"]
# roots << "#{home}/Desktop"

tags = []

roots.each do |root|
  Find.find(root) do |path|
    next if FileTest.directory? path
    next unless %w(.mp3 .ogg .wma .flac .aac .mp4 .m4a).include?(File.extname(path))
    puts path
    AudioInfo.open(path) do |f|
      begin
        next if f.artist.nil? || f.artist.empty?
        artist = f.artist.strip
        puts artist
        tags << artist
      rescue
        puts 'error!'
        next
      end
    end
  end
end

tags.uniq!

puts tags
puts "Found #{tags.size} Artists"

File.open('output.csv', 'w') do |f|
  begin
    f.puts(tags)
  rescue => e
    puts 'error'
    puts e
  end
end
